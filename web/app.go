package main

import (
	"github.com/go-pg/pg/v10"
	"github.com/gorilla/mux"
	"gitlab.com/beginbot/stylist/pkg/colorscheme"
	"gitlab.com/beginbot/stylist/pkg/database"
	"html/template"
	"net/http"
)

var db *pg.DB
var index_tmpl *template.Template
var theme_tmpl *template.Template

type ThemePage struct {
	VotesByUser []colorscheme.VoteCount
	Domain      string
	Theme       string
	Count       int
	Color0      string
	Color1      string
	Color2      string
	Color3      string
	Color4      string
	Color5      string
	Color6      string
	Color7      string
	Color8      string
	Color9      string
	Color10     string
	Color11     string
	Color12     string
	Color13     string
	Color14     string
	Color15     string
}

func ThemeHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	theme := vars["theme"]
	count := colorscheme.CountByTheme(db, theme)
	// count := dal.ThemeCount(r.Context(), db, theme)

	cs := colorscheme.ParseColorScheme(theme)
	votesByUser := colorscheme.VotesByUser(db, theme)

	page := ThemePage{
		VotesByUser: votesByUser,
		Domain:      "http://localhost:1992",
		Theme:       theme,
		Count:       count,
		Color0:      cs.Colors.Color0,
		Color1:      cs.Colors.Color1,
		Color2:      cs.Colors.Color2,
		Color3:      cs.Colors.Color3,
		Color4:      cs.Colors.Color4,
		Color5:      cs.Colors.Color5,
		Color6:      cs.Colors.Color6,
		Color7:      cs.Colors.Color7,
		Color8:      cs.Colors.Color8,
		Color9:      cs.Colors.Color9,
		Color10:     cs.Colors.Color10,
		Color11:     cs.Colors.Color11,
		Color12:     cs.Colors.Color12,
		Color13:     cs.Colors.Color13,
		Color14:     cs.Colors.Color14,
		Color15:     cs.Colors.Color15}

	theme_tmpl.Execute(w, page)
}

type HomePage struct {
	TotalCount    int
	Domain        string
	ThemesByCount []colorscheme.ThemeCount
}

func HomeHandler(w http.ResponseWriter, r *http.Request) {
	totalVoteCount := colorscheme.Count(db)
	themesByCount := colorscheme.ThemesByCount(db)

	w.WriteHeader(http.StatusOK)

	home := HomePage{
		TotalCount:    totalVoteCount,
		Domain:        "http://localhost:1992",
		ThemesByCount: themesByCount}

	index_tmpl.Execute(w, home)
}

func init() {
	// skippednote: You should try ParseGlob now that you have multiple templates.
	db = database.CreatePgDbConn()
	index_tmpl = template.Must(template.ParseFiles("templates/index.html"))
	theme_tmpl = template.Must(template.ParseFiles("templates/theme.html"))
}

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/", HomeHandler)
	r.HandleFunc("/theme/{theme}", ThemeHandler)
	http.Handle("/", r)

	http.ListenAndServe(":1992", r)

	for {
	}
}
