package main

import (
	"context"
	"fmt"
	"gitlab.com/beginbot/stylist/pkg/chat"
	"gitlab.com/beginbot/stylist/pkg/colorscheme"
	"gitlab.com/beginbot/stylist/pkg/config"
	"gitlab.com/beginbot/stylist/pkg/database"
	"gitlab.com/beginbot/stylist/pkg/executor"
	"gitlab.com/beginbot/stylist/pkg/filter"
	"gitlab.com/beginbot/stylist/pkg/irc"
	"gitlab.com/beginbot/stylist/pkg/reporter"
	"gitlab.com/beginbot/stylist/pkg/router"
	"io/ioutil"
	"log"
	"path/filepath"
	"strings"
)

func main() {
	fmt.Println("Stylist Time! !color to change color in IRC chat")

	pgdb := database.CreatePgDbConn()
	defer pgdb.Close()

	// Where should Schema Creation go
	err := colorscheme.CreateSchema(pgdb)
	if err != nil {
		panic(err)
	}
	c := config.NewConfig()
	ctx := context.Background()

	err = pgdb.Ping(ctx)
	if err != nil {
		log.Fatal("We can't even PING the database!")
	}

	dir := "/home/begin/code/pywal/pywal/colorschemes/dark"
	files, err := ioutil.ReadDir(dir)

	if err != nil {
		log.Fatal(err)
	}

	hatedColors := []string{"base16-icy", "base16-greenscreen", "dkeg-owl"}

	themes := make([]string, len(files))

Loop:
	for i, f := range files {

		n := f.Name()
		name := strings.TrimSuffix(filepath.Base(n), filepath.Ext(n))

		for _, color := range hatedColors {
			if color == name {
				fmt.Println("We found a hated colored: ", color)
				continue Loop
			}
		}

		themes[i] = name
	}

	// We should eliminate some themes here or filter later
	// Launch a GoRoutine that collect messages from IRC
	messages := irc.ReadIrc(ctx, c.Conn)

	// Split the Messages into User Messages and if we should Pong IRC
	userMsgs, dataStream, pongPlz := router.Router(ctx, messages)

	// Save the Chat in the DB
	chat.SaveChat(ctx, pgdb, dataStream)

	// Filter out all messages from users except for commands
	commands := filter.Filter(ctx, userMsgs)

	// Execute the user commands and return the results
	results := executor.Execute(ctx, pgdb, commands, themes)

	// Report the Results of executing commands
	reporter.Report(ctx, results, &c)

	// Start an infinite loop
	// waiting for either the loop to get a Done message
	// or for message to send a Pong to the IRC Server
	for {
		select {
		case <-ctx.Done():
		case <-pongPlz:
			irc.Pong(&c)
		}
	}
}
