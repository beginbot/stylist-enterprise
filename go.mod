module gitlab.com/beginbot/stylist

go 1.15

require (
	github.com/go-pg/pg/v10 v10.0.0-beta.10
	github.com/gorilla/mux v1.8.0
	github.com/lib/pq v1.8.0
)
