package colorscheme

import (
	"encoding/json"
	"fmt"
	"github.com/go-pg/pg/v10"
	"github.com/go-pg/pg/v10/orm"
	"gitlab.com/beginbot/stylist/pkg/models"
	"io/ioutil"
)

type Special struct {
	Background string `json:"background"`
	Foreground string `json:"foreground"`
	Cursor     string `json:"cursor"`
}

func (c Colorscheme) String() string {
	return fmt.Sprintf("Color<%s>", c.Theme)
}

type Colors struct {
	Color0  string `json:"color0"`
	Color1  string `json:"color1"`
	Color2  string `json:"color2"`
	Color3  string `json:"color3"`
	Color4  string `json:"color4"`
	Color5  string `json:"color5"`
	Color6  string `json:"color6"`
	Color7  string `json:"color7"`
	Color8  string `json:"color8"`
	Color9  string `json:"color9"`
	Color10 string `json:"color10"`
	Color11 string `json:"color11"`
	Color12 string `json:"color12"`
	Color13 string `json:"color13"`
	Color14 string `json:"color14"`
	Color15 string `json:"color15"`
}

type Colorscheme struct {
	Theme   string
	Special Special
	Colors  Colors
}

func All(db *pg.DB) []models.ColorschemeVote {
	var colorschemes []models.ColorschemeVote

	err := db.Model((*models.ColorschemeVote)(nil)).
		Select(&colorschemes)

	if err != nil {
		fmt.Println("Error for All: ", err)
	}

	return colorschemes
}

func WinningColor(db *pg.DB) (string, int) {
	var res []ThemeCount

	err := db.Model((*models.ColorschemeVote)(nil)).
		Column("theme").
		ColumnExpr("count(*) AS count").
		Group("theme").
		Order("count DESC").
		Limit(1).
		Select(&res)

	if err != nil {
		fmt.Println("Error Winning Color: ", err)
	}

	return res[0].Theme, res[0].Count
}

func CountByTheme(db *pg.DB, theme string) int {
	var colorschemes []models.ColorschemeVote
	err := db.Model(&colorschemes).Where("theme = ?", theme).Select()

	if err != nil {
		fmt.Println("Error querying ", err)
		return 0
	}
	return len(colorschemes)
}

type ThemeCount struct {
	Theme string
	Count int
}

type VoteCount struct {
	Username string
	Count    int
}

func VotesByUser(db *pg.DB, theme string) []VoteCount {
	var results []VoteCount

	db.Model((*models.ColorschemeVote)(nil)).
		Where("theme = ?", theme).
		Column("username").
		ColumnExpr("count(*) AS count").
		Group("username").
		Order("count DESC").
		Select(&results)

	return results
}

func ThemesByCount(db *pg.DB) []ThemeCount {
	var res []ThemeCount

	db.Model((*models.ColorschemeVote)(nil)).
		Column("theme").
		ColumnExpr("count(*) AS count").
		Group("theme").
		Order("count DESC").
		Select(&res)

	return res
}

func Count(db *pg.DB) int {
	var colorschemes []models.ColorschemeVote
	err := db.Model(&colorschemes).Select()
	if err != nil {
		panic(err)
	}

	return len(colorschemes)
}

func Create(db *pg.DB, colorschemeVote models.ColorschemeVote) {
	_, err := db.Model(&colorschemeVote).Insert()
	if err != nil {
		panic(err)
	}
}

func CreateSchema(db *pg.DB) error {
	models := []interface{}{
		(*Colorscheme)(nil),
	}

	for _, model := range models {
		err := db.Model(model).CreateTable(&orm.CreateTableOptions{
			Temp: true,
		})
		if err != nil {
			return err
		}
	}
	return nil
}

func ParseColorScheme(theme string) Colorscheme {
	var colorscheme Colorscheme
	colorscheme.Theme = theme
	filename := fmt.Sprintf("/home/begin/code/pywal/pywal/colorschemes/dark/%s.json", theme)
	colorData, _ := ioutil.ReadFile(filename)
	err := json.Unmarshal(colorData, &colorscheme)

	if err != nil {
		fmt.Println("ERRORS ", err)
	}

	return colorscheme
}
