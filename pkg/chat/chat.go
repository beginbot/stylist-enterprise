package chat

import (
	"context"
	"fmt"
	"github.com/go-pg/pg/v10"
	"github.com/go-pg/pg/v10/orm"
	// "time"
)

type ChatMessage struct {
	Username string
	Message  string
	// CreatedAt time.Duration
}

func Save(db *pg.DB, chat_message ChatMessage) {
	_, err := db.Model(&chat_message).Insert()

	if err != nil {
		fmt.Println("Err Saving Message: ", err)
		// panic(err)
	}

}

func CreateSchema(db *pg.DB) error {
	models := []interface{}{
		(*ChatMessage)(nil),
	}

	for _, model := range models {
		err := db.Model(model).CreateTable(&orm.CreateTableOptions{
			Temp: true,
		})
		if err != nil {
			return err
		}
	}
	return nil
}

func SaveChat(ctx context.Context, db *pg.DB, dataStream <-chan ChatMessage) {
	go func() {

		for msg := range dataStream {
			select {
			case <-ctx.Done():
				return
			default:
				Save(db, msg)
			}
		}

	}()

}
