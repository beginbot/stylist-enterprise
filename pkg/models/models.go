package models

type ChatMsg struct {
	Username string
	Message  string
}

type ColorschemeVote struct {
	Username string
	Theme    string
}
