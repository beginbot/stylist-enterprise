package router

import (
	"context"
	"gitlab.com/beginbot/stylist/pkg/chat"
	"gitlab.com/beginbot/stylist/pkg/parser"
)

// Declaring something on the outside of a function here

func Router(ctx context.Context, messages <-chan string) (<-chan chat.ChatMessage, <-chan chat.ChatMessage, <-chan bool) {

	// UsersWeDoNotLike := []string{"nightbot", "beginbot"}
	UsersWeDoNotLike := []string{"nightbot", "beginbotbot", "beginbot"}

	userMsgs := make(chan chat.ChatMessage)
	dataStream := make(chan chat.ChatMessage)
	pongPlz := make(chan bool, 1)

	go func() {
		defer close(userMsgs)
		defer close(dataStream)
		defer close(pongPlz)

		for msg := range messages {
		outside:

			select {
			case <-ctx.Done():
				return
			default:

				if parser.IsPrivmsg(msg) {
					user, m := parser.ParsePrivmsg(msg)
					chat_message := chat.ChatMessage{Username: user, Message: m}

					userMsgs <- chat_message

					for _, u := range UsersWeDoNotLike {
						if u == user {
							break outside
						}
					}
					dataStream <- chat_message
				} else if parser.IsPing(msg) {
					pongPlz <- true
				}

			}
		}
	}()

	return userMsgs, dataStream, pongPlz
}
