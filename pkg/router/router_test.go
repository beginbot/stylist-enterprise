package router

import (
	"context"
	"fmt"
	"testing"
)

func TestRouter(t *testing.T) {
	done := make(chan interface{})
	inputChan := make(chan string, 2)
	inputChan <- ":tmi.twitch.tv 003 beginbotbot :This server is rather new"
	inputChan <- ":stupac62!tmi.twitch.tv PRIVMSG #beginbot :@Mccannch just a side benefit haha"
	t.Log(inputChan)
	t.Log(done)

	fmt.Println("About to filter")
	outputChan := Router(done, inputChan)
	fmt.Println("filtered")
	result1 := <-outputChan
	close(done)
	if result1 != ":stupac62!tmi.twitch.tv PRIVMSG #beginbot :@Mccannch just a side benefit haha" {
		t.Errorf("Did Not Find PRIVMSG: %s", result1)
	}

	// Testing Pattern if something is blocking
	select {
	case <-outputChan:
		t.Errorf("THIS WASN'T SUPPOSED TO HAPPEN!!!!")
	default:
		t.Log("All is good, we blocking")
	}

}
