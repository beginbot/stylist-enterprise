package database

import (
	"database/sql"
	"github.com/go-pg/pg/v10"
	_ "github.com/lib/pq"
	"log"
)

func CreateDbConn() *sql.DB {
	connStr := "user=begin dbname=postgres sslmode=disable"
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		log.Fatal(err)
	}
	return db
}

func CreatePgDbConn() *pg.DB {
	db := pg.Connect(&pg.Options{
		Addr:     ":5432",
		User:     "begin",
		Database: "postgres",
	})
	return db
}
