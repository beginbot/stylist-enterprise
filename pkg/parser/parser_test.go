package parser

import (
	"testing"
)

func TestIsPing(t *testing.T) {
	data := "PING :tmi.twitch.tv"
	result := IsPing(data)

	if !result {
		t.Error("We did not Find PING")
	}

}
