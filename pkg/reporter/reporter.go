package reporter

import (
	"context"
	"fmt"
	"gitlab.com/beginbot/stylist/pkg/config"
	"gitlab.com/beginbot/stylist/pkg/irc"
)

func Report(ctx context.Context, messages <-chan string, c *config.Config) {
	go func() {
		for msg := range messages {
			select {
			case <-ctx.Done():
				return
			default:
				irc.SendMsg(c, msg)
				fmt.Println(msg)
			}
		}
	}()
}
