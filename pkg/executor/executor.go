package executor

import (
	"context"
	"fmt"
	"github.com/go-pg/pg/v10"
	"gitlab.com/beginbot/stylist/pkg/chat"
	"gitlab.com/beginbot/stylist/pkg/colorscheme"
	"gitlab.com/beginbot/stylist/pkg/models"
	"math/rand"
	"os/exec"
	"strings"
	"time"
)

func Execute(ctx context.Context, pgdb *pg.DB, messages <-chan chat.ChatMessage, themes []string) <-chan string {
	results := make(chan string)

	go func() {
		defer close(results)

		for msg := range messages {
			select {
			case <-ctx.Done():
				return
			default:
			}

			msgBreakdown := strings.Split(msg.Message, " ")
			cmd := strings.ToLower(strings.TrimSpace(msgBreakdown[0][1:]))

			if cmd == "colors" {
				results <- "https://gist.github.com/dylanaraps/3c688fe1db75838e52275264182b1ead"
				continue
			}
			if cmd == "color" {
				color := colorFinder(msg.Message)

				if color == "" {
					rand.Seed(time.Now().UTC().UnixNano())
					i := rand.Intn(len(themes))
					color := themes[i]
					res := fmt.Sprintf("User @%s Voted for Random Color: %s\n", msg.Username, color)
					fmt.Println(res)
					results <- res
					ExecColor(ctx, pgdb, results, msg, color)
					continue
				}

				for _, c := range themes {
					if c == color {
						fmt.Println("Match Found!", c)
						ExecColor(ctx, pgdb, results, msg, color)
					}
				}

			}

		}
	}()

	return results
}

func colorFinder(msg string) string {
	return strings.TrimSpace(strings.Join(strings.Split(msg, " ")[1:], " "))
}

func ExecColor(ctx context.Context, pgdb *pg.DB, results chan<- string, msg chat.ChatMessage, color string) {
	vote := models.ColorschemeVote{Theme: color, Username: msg.Username}
	colorscheme.Create(pgdb, vote)

	themeCount := colorscheme.CountByTheme(pgdb, color)
	fmt.Println("ThemeCount: ", themeCount)
	tc, tcc := colorscheme.WinningColor(pgdb)

	fmt.Println("-----------------------")
	fmt.Println("User: ", msg.Username)
	fmt.Printf("Color: %s | Count: %d\n", color, themeCount)
	fmt.Printf("Top Color: %s | Count: %d\n", tc, tcc)
	fmt.Println("-----------------------")

	if themeCount >= tcc {
		fmt.Println("Changing Color: ", color)
		results <- fmt.Sprintf("User @%s Changed Color to: %s", msg.Username, color)
		changeColorScheme(color)
	} else {
		fmt.Println("NOT CHANGING COLOR: ", color)
	}
}

func changeColorScheme(color string) {
	if color == "" {
		color = "random_dark"
	}

	fmt.Println("Color: ", color)
	exec.Command("scripts/colorscheme.sh", color).Output()
}
