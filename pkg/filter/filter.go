package filter

import (
	"context"
	"fmt"
	"gitlab.com/beginbot/stylist/pkg/chat"
	"gitlab.com/beginbot/stylist/pkg/parser"
)

func Filter(ctx context.Context, messages <-chan chat.ChatMessage) <-chan chat.ChatMessage {
	commands := make(chan chat.ChatMessage, 10)

	usersWeHate := []string{"beginbotbot"}

	go func() {
		defer close(commands)

	outside:
		for msg := range messages {
			select {
			case <-ctx.Done():
				return
			default:
				if parser.IsCommand(msg.Message) {

					for _, u := range usersWeHate {
						if u == msg.Username {
							fmt.Println("Bad User: ", u)
							continue outside
						}
					}
					commands <- msg

				}
			}
		}

	}()

	return commands
}
