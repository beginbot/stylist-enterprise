CREATE TABLE public.chat_messages(
    id SERIAL,
    username character varying(255) NOT NULL,
    message character varying(255) NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    CONSTRAINT chat_messages_pkey PRIMARY KEY (id)
);

CREATE TABLE public.colorscheme_votes(
    id SERIAL,
    username character varying(255) NOT NULL,
    theme character varying(255) NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    CONSTRAINT colorscheme_votes_pkey PRIMARY KEY (id)
);
